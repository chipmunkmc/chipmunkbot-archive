# chipmunkbot-archive
Archive of ChipmunkBot, a [kaboom](https://kaboom.pw) bot from 2022 (before the rewrite). The code should run with all modules installed, however, continue at your own risk, as the bot has a (sandboxed) public eval command, the security of which cannot be guaranteed. The bot's current chat parser is known to be broken on modern minecraft servers.
